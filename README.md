# README

### Versões
- Ruby 2.6.0
- Rails 6.0.3

### Clone o repositório
- git clone git@gitlab.com:isabelademattos/backend-ruby.git
- cd backend-ruby
- bundle install
- rake db:create db:migrate db:seed
