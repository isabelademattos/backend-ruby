class Animal < ApplicationRecord
  belongs_to :person

  validate :allowed_to_have_swallows
  validate :allowed_to_have_cats
  validate :allowed_to_have_animals

  scope :dogs_cost_average, -> { where('animal_type = ?', 'Cachorro').average(:mensal_cost) }
  scope :dogs_count, -> { where('animal_type = ?', 'Cachorro').count }

  def allowed_to_have_swallows
    return true unless animal_type == 'Andorinha'
    errors.add(:base, "Pessoas devem ter mais de 18 anos para ter andorinhas") if Time.now.year - person.birth_date.year < 18
  end

  def allowed_to_have_cats
    return true unless animal_type == 'Gato'
    errors.add(:base, "Pessoas que tenham nome começando com a letra 'A' não podem ter animais do tipo Gato") if ['A', 'a'].include? person.name[0]
  end

  def allowed_to_have_animals
    errors.add(:base, "Pessoas que já tiverem custos com animais acima de 1000 não podem ter mais animais") if person.animals.sum(:mensal_cost) > 1000
  end
end
