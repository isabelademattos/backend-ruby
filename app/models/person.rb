class Person < ApplicationRecord
  has_many :animals

  scope :dogs_owners_name, -> { joins(:animals).where('animals.animal_type = ?', 'Cachorro').order(:name).pluck(:name) }
  scope :mensal_cost_order, -> { joins(:animals).group(:name).order('SUM(animals.mensal_cost) DESC, name').pluck(:name, 'SUM(animals.mensal_cost)') }
  scope :three_month_mensal_cost, -> { joins(:animals).group(:name).order('(SUM(animals.mensal_cost)*3) DESC, name').pluck(:name, 'SUM(animals.mensal_cost)*3') }
end
